const nr = require("newrelic");
const express = require("express");
const cookieSession = require("cookie-session");
const passport = require("passport");
const path = require("path");
const mongoose = require("mongoose");
const keys = require("./config/keys");
const expressValidator = require("express-validator");
const moment = require("moment");

//const { WebClient } = require('@slack/client');
const { WebClient } = require("@slack/web-api");
const web = new WebClient(keys.slack.slackBotToken);
let Channel = require("./models/channel");
const User = require("./models/user");
const Task = require("./models/task");
const resetStreakCronJob = require("./utils/resetStreakCron");

mongoose.connect(keys.mongodb.dbURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
let db = mongoose.connection;

// Check connection
db.once("uri", function () {
  console.log("Connected to MongoDB");
});

// Check for DB errors
db.on("error", function (err) {
  console.log(err);
});

//Routes
const index = require("./routes/index");
const users = require("./routes/users");
const bot = require("./routes/bot");
const channels = require("./routes/channels");
const tasks = require("./routes/tasks");
const welcome = require("./routes/welcome");
const resources = require("./routes/resources");

// Init app
const app = express();

(async () => {
  //console.log(JSON.stringify());
})();

//Passport config
require("./config/passport")(passport);
// view engine setup
app.set("views", path.join(__dirname, "views"));
// set view engine
app.set("view engine", "ejs");

// initialize passport
app.use(passport.initialize());
app.use(passport.session());

// Set Public Folder
app.use(express.static(path.join(__dirname, "public")));

// Body Parser Middleware
// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
// parse application/json
app.use(express.json());

// set up session cookies
app.use(
  cookieSession({
    maxAge: 24 * 60 * 60 * 1000,
    keys: [keys.session.cookieKey],
  })
);

// Express Messages Middleware
app.use(require("connect-flash")());
app.use(function (req, res, next) {
  res.locals.messages = require("express-messages")(req, res);
  next();
});

// initialize passport
app.use(passport.initialize());
app.use(passport.session());

app.get("*", function (req, res, next) {
  res.locals.user = req.user || null;
  res.locals.errors = res.locals.errors || null;
  res.locals.userPro = res.locals.userPro || null;
  next();
});
app.post("/channels/events", (req, res, next) => {
  if (req.body.type == "event_callback" && req.body.event.type == "team_join") {
    req.url = "/welcome/events";
    next();
  } else {
    next();
  }
});
app.post("/bot/interactive", (req, res, next) => {
  console.log(`bot interactive ${req.body.payload}`);
  var payload = JSON.parse(req.body.payload);
  if (
    payload.callback_id == "channels" ||
    payload.callback_id == "howtotasks"
  ) {
    req.url = "/welcome/interactive";
    console.log("middleware if");
    next();
  } else {
    next();
  }
});

// Express Validator Middleware
app.use(
  expressValidator({
    errorFormatter: function (param, msg, value) {
      var namespace = param.split("."),
        root = namespace.shift(),
        formParam = root;

      while (namespace.length) {
        formParam += "[" + namespace.shift() + "]";
      }
      return {
        param: formParam,
        msg: msg,
        value: value,
      };
    },
  })
);

// Set up routes

app.use("/users", users);
app.use("/bot", bot);
app.use("/channels", channels);
app.use("/tasks", tasks);
app.use("/welcome", welcome);
app.use("/resources", resources);
app.use("/", index);

// Start streak reset cron job
resetStreakCronJob.start();

//Retrieve list of channels from slack api
async function getChannelList() {
  try {
    //web.channels.list()
    const channelRes = await Channel.findOne({ channelName: "general" });
    if (!channelRes) {
      const res = await web.conversations.list();
      //console.log(`channels: ${JSON.stringify(res)}`);
      console.log(`channels: ${res}`);

      res.channels.forEach(async (c) => {
        try {
          if (c.name == "general") {
            const channel = new Channel({
              channelName: c.name,
              channelID: c.id,
            });

            const doc = await channel.save();
            console.log(`doccc ${doc}`);
          }
        } catch (error) {
          console.log(`err: ${error}`);
        }
      });
    }
  } catch (error) {
    console.log(`err: ${error}`);
  }
}
getChannelList();
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  /*  res.locals.message = err.message;
	  res.locals.error = req.app.get('env') === 'development' ? err : {};
	*/
  // render the error page
  res.locals.message = err.message;
  res.locals.error = err;

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});
module.exports = app;
