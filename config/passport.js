const passport = require('passport');
const SlackStrategy = require('@aoberoi/passport-slack').default.Strategy;
const keys = require('./keys');
const User = require('../models/user');
const Channel = require('../models/channel');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id).then((user) => {
        done(null, user);
    });
});



module.exports = function(passport){
	passport.use(new SlackStrategy({
		clientID: keys.slack.clientID,
		clientSecret: keys.slack.clientSecret,
		//team: keys.slack.teamID,
		authorizationURL: keys.slack.slackUrl+'/oauth/authorize'
	}, (accessToken, scopes, team,extra, { user: userProfile , team: teamProfile }, done) => {
		//console.log('Slack Strategy'+ accessToken+scopes+JSON.stringify(userProfile));
		// check if user already exists in our own db
        User.findOne({slackID: userProfile.id}).then((currentUser) => {
            if(currentUser){
                // already have this user
                //console.log('user is: ', currentUser);
                done(null, currentUser);
            } else {
                // if not, create user in our db
                Channel.find({channelName:'general'},(err,channelRes)=>{
                    new User({
                        slackID: userProfile.id,
                        name: userProfile.name,
                        email: userProfile.email,
                        img: userProfile.image_512
                    }).save().then((newUser) => {
                     User.update({"_id":newUser._id},{ "$addToSet": { channel: channelRes[0] } },function (err, raw) {
                        if (err) return handleError(err);
                        console.log('The raw response from Mongo was ', raw);
                    });
                     
                     console.log('created new user: ', newUser);
                     done(null, newUser);
                 });    
                });
                
            }
        });
    }));

}
