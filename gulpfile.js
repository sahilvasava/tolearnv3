const gulp = require('gulp');
const sass = require('gulp-sass');
//const nodemon = require('gulp-nodemon');

function styles() {
	return gulp.src(['public/stylesheets/scss/*.scss'])
		.pipe(sass())
		.pipe(gulp.dest("public/stylesheets/css"));
}
// Compile Sass & Inject Into Browser
gulp.task('sass', gulp.series(styles));
/* gulp.task('sass', function () {
	return gulp.src(['public/stylesheets/scss/*.scss'])
		.pipe(sass())
		.pipe(gulp.dest("public/stylesheets/css"));
}); */
/* gulp.task('nodemon', ['sass'], function (cb) {

	var started = false;

	return nodemon({
		script: './bin/www'
	}).on('start', function () {
		// to avoid nodemon being started multiple times
		// thanks @matthisk
		if (!started) {
			cb();
			started = true;
		}
	});
}); */

//gulp.watch(['public/stylesheets/scss/*.scss'], ['sass']);

// Default Task
gulp.task('default', gulp.series(styles));