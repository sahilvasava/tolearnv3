const mongoose = require('mongoose');

// List Schema
const ChannelSchema = mongoose.Schema({
  channelName:{
    type: String
  },
  channelID:{
    type: String,
    unique: true
  }
});

const Channel = module.exports = mongoose.model('Channel', ChannelSchema);
