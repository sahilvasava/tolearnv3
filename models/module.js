const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Module Schema
const ModuleSchema = mongoose.Schema({
    title:{
      type: String
    },
    courses:[
        {
            title: String,
            url: String
        }
    ]
  });
  
  
  const Module = module.exports = mongoose.model('Module', ModuleSchema);