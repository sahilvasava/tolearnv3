const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Resource Schema
const ResourceSchema = mongoose.Schema({
  title: {
    type: String
  },
  modules: [
    {
      type: Schema.Types.ObjectId,
      ref: "Module"
    }
  ],
  created_at: {
    type: Date,
    default: Date.now
  },
  created_by: {
    type: Schema.Types.ObjectId,
    ref: "User"
  }
});

const Resource = (module.exports = mongoose.model("Resource", ResourceSchema));
