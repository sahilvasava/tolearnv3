const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('./user');
const channel = require('./channel');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
// Task Schema
const TaskSchema = mongoose.Schema({
  task:{
    type: String
  },
  channel: {
    type: Schema.Types.ObjectId,
    ref:'Channel'
  },
  user: {
    type: Schema.Types.ObjectId,
    ref:'User'
  },
  submitDate: {
    type: Date,
    default: null
  },
  doneDate: {
    type: Date,
    default: null
  }
});

TaskSchema.plugin(mongooseAggregatePaginate);

const Task = module.exports = mongoose.model('Task', TaskSchema);