const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

// User Schema
const UserSchema = mongoose.Schema({
  name:{
    type: String
  },
  email:{
    type: String,
    unique: true
  },
  slackID: {
    type: String
  },
  img: {
    type:String
  },
  channel:[{
    type: Schema.Types.ObjectId,
    ref:'Channel'
  }],
  streak: {
    type: Number,
    default: 0
  }
});

UserSchema.plugin(mongooseAggregatePaginate);
const User = module.exports = mongoose.model('User', UserSchema);