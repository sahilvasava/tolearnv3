$(document).ready(function(){
	$(".joinModalButton").on('click',function(){
      var target = "#"+ $(this).data("target");
      $(target).addClass("is-active");
    });
    $(".modal-background, .modal-close").on('click',function(){
      var target = "#"+ $(this).data("target");
      $(target).removeClass("is-active");
    });
});