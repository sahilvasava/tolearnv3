/*document.addEventListener('DOMContentLoaded', function () {

  // Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(function ($el) {
      $el.addEventListener('click', function () {

        // Get the target from the "data-target" attribute
        var target = $el.dataset.target;
        var $target = document.getElementById(target);

        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
        $el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }

});*/

$(document).ready(function(){
  let pageNo = 1;
  let total,z=1;
  let ajaxRunning = false;
  loadTasks(pageNo);
  pageNo++;
  $(document).ajaxStart(function(e, xhr, opt){
    $(".column_card").append("<div class='has-text-centered spinner_ico'><i class='fas fa-spinner fa-spin'></i></div>");
    ajaxRunning = true;
  }).ajaxStop(function(e, xhr, opt){
    $("div").remove(".spinner_ico");
    ajaxRunning = false;
  });
  

  $(window).scroll(function(){
    //console.log("Doc: "+document.body.clientHeight+'\nWin: '+$(window).height()+'\nWintop: '+$(window).scrollTop());
    //console.log($(window).scrollTop()  >= (document.body.clientHeight - $(window).height()-100));
    //$(window).scrollTop() >= $(document).height() - $(window).height()-10)
    var docHeight = document.body.offsetHeight;
    docHeight = docHeight == undefined ? window.document.documentElement.scrollHeight : docHeight;

    var winheight = window.innerHeight;
    winheight = winheight == undefined ? document.documentElement.clientHeight : winheight;

    var scrollpoint = window.scrollY;
    scrollpoint = scrollpoint == undefined ? window.document.documentElement.scrollTop : scrollpoint;
    console.log("scrollpoint: "+scrollpoint+"\nwinheight: "+winheight+"\ndocheight: "+docHeight);
    console.log((scrollpoint + winheight) >= docHeight);
    if  (!ajaxRunning && ((scrollpoint + winheight+20) >= docHeight)){
      if(pageNo>total){
        return false;
      } else {
        loadTasks(pageNo);
        pageNo++;
      }
    } else {return false;}
  });
    /*if($(window).height() >= $(document).height()){
        
    }*/
    let no_scrollbar_workaround = setInterval(function checkVariable() {

     if($(window).height() >= $(document).height()) {
      if (pageNo>total) {
        clearInterval(no_scrollbar_workaround);
        return false;
      } else {
        loadTasks(pageNo);

        pageNo++;
      }
    }
  }, 1000);
    
    
    function loadTasks(pageNumber){


      $.post("/tasks/fetch?pageNo="+pageNumber,function(data,status){
        total = data.pageCount;
        if(total){
          console.log(data);
          console.log(data.results[0]._id.date);
        /*let date = moment().calendar("2018-03-18", {
          sameDay: '[<strong>Today</strong>], MMMM Do',
          lastDay: '[<strong>Yesterday</strong>], MMMM Do',
          sameElse: '[<strong>]dddd[</strong>], MMMM Do'
        });*/
        let yesterday = new Date();
        yesterday.setDate(yesterday.getDate()-1);
        let date;
        let zonename = moment.tz.guess();
        console.log(yesterday.getFullYear()+'-'+(yesterday.getMonth()+1)+'-'+yesterday.getDate())
        if(new Date().toDateString() == new Date(data.results[0]._id.date).toDateString()){
          date = moment.tz(data.results[0]._id.date,zonename).format("[<strong>Today</strong>,] MMMM Do");
        } else if(yesterday.toDateString() == new Date(data.results[0]._id.date).toDateString()) {
          date = moment.tz(data.results[0]._id.date,zonename).format("[<strong>Yesterday</strong>,] MMMM Do");
          console.log(date);
        } else {
          date = moment.tz(data.results[0]._id.date,zonename).format("[<strong>]dddd[</strong>], MMMM Do");
        }
        
        
        let userRecords = data.results[0].records,x,y;
        //let imgSrc = data.results[0].rec
        console.log("userRecords: "+userRecords[0]._id.user_id[0].name);
        console.log(date);
        $(".card_parent").append("<div class='card'><header class='card-header'><p class='card_date'>"+date+"</p></header><div class='card-content "+moment.tz(data.results[0]._id.date,zonename).format("DMY")+"'>");
        console.log("userRecords:"+userRecords);
        for(x in userRecords){
          $("."+moment.tz(data.results[0]._id.date,zonename).format("DMY")).append('<div class="user_content" id="user_content'+z+'"><div class="media"><div class="media-left"><a href="/'+userRecords[x]._id.user_id[0].slackID+'"><figure class="image user_img"><img src="'+userRecords[x]._id.user_id[0].img+'" alt="Img"></a></figure></div><div class="media-content"><a href="/'+userRecords[x]._id.user_id[0].slackID+'"><p class="userTask_name">'+userRecords[x]._id.user_id[0].name+'</p></a></div></div>');
          for(y in userRecords[x].tasks){
            if(/<http.*>/i.test(userRecords[x].tasks[y].task)){
              let re = /<http.*>/ig;
              let arr;
              let taskWithUrl;
              while((arr = re.exec(userRecords[x].tasks[y].task)) !== null){
                let arrItem = arr[0];
                if(/<http.*\|.*>/.test(arrItem)){
                  taskWithUrl =  userRecords[x].tasks[y].task.replace(arr[0],'<a href="'+/[^<][^|]*/.exec(arrItem)+'">'+/[^<][^|]*/.exec(arrItem)+'</a>');  
                } else{
                  taskWithUrl =  userRecords[x].tasks[y].task.replace(arr[0],'<a href="'+/[^<].*[^>]/.exec(arrItem)+'">'+/[^<].*[^>]/.exec(arrItem)+'</a>');  
                }
                
              }
              $("#user_content"+z).append('<p class="task"><i class="fas fa-check tick_icon"></i>&nbsp;'+taskWithUrl+'&nbsp;<a class="userTask_channel" href="'+data.slackurl+'/messages/'+userRecords[x].tasks[y].channel[0].channelID+'" target="_blank">#'+userRecords[x].tasks[y].channel[0].channelName+'</a>&nbsp;<small>'+moment.tz(userRecords[x].tasks[y].doneDate,zonename).fromNow()+'</small></p>');
            } else {
              $("#user_content"+z).append('<p class="task"><i class="fas fa-check tick_icon"></i>&nbsp;'+userRecords[x].tasks[y].task+'&nbsp;<a class="userTask_channel" href="'+data.slackurl+'/messages/'+userRecords[x].tasks[y].channel[0].channelID+'" target="_blank">#'+userRecords[x].tasks[y].channel[0].channelName+'</a>&nbsp;<small>'+moment.tz(userRecords[x].tasks[y].doneDate,zonename).fromNow()+'</small></p>');  
            }
            
          }
          z++;
          $("."+moment.tz(data.results[0]._id.date,zonename).format("DMY")).append('</div>');
        } 
        $(".card").append("</div>");
        $(".card_parent").append("</div>");
      }
    });
      

    }
    
    $(".joinModalButton").on('click',function(){
      var target = "#"+ $(this).data("target");
      $(target).addClass("is-active");
    });
    $(".modal-background, .modal-close").on('click',function(){
      var target = "#"+ $(this).data("target");
      $(target).removeClass("is-active");
    });

   

  });