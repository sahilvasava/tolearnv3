$(document).ready(function(){
	let url = $('.pending').attr('href');
	let pageNo = 1;
	let total;
	let ajaxRunning = false;
	loadTasks(pageNo);
	pageNo++;
	$(document).ajaxStart(function(e, xhr, opt){
		$(".pending_list").append("<div class='has-text-centered spinner_ico'><i class='fas fa-spinner fa-spin'></i></div>");
		ajaxRunning = true;
	}).ajaxStop(function(e, xhr, opt){
		$("div").remove(".spinner_ico");
		ajaxRunning = false;
	});
	console.log("Doc: "+$(document).height()+'\nWin: '+$(window).height()+'\nWin: '+$(window).scrollTop());
	$(window).scroll(function(){
		var docHeight = document.body.offsetHeight;
		docHeight = docHeight == undefined ? window.document.documentElement.scrollHeight : docHeight;

		var winheight = window.innerHeight;
		winheight = winheight == undefined ? document.documentElement.clientHeight : winheight;

		var scrollpoint = window.scrollY;
		scrollpoint = scrollpoint == undefined ? window.document.documentElement.scrollTop : scrollpoint;
		if  (!ajaxRunning && ((scrollpoint + winheight) >= docHeight)){
			if(pageNo>total){
				return false;
			} else {
				loadTasks(pageNo);
				pageNo++;
			}
		} else {return};
	});

	let no_scrollbar_workaround = setInterval(function checkVariable() {

		if($(window).height() >= $(document).height()) {
			if (pageNo>total) {
				clearInterval(no_scrollbar_workaround);
				return false;
			} else {
				loadTasks(pageNo);

				pageNo++;
			}
		}
	}, 1000);
	console.log(url);
	function loadTasks(pageNo){
		$.getJSON(url+'tasks?pageNo='+pageNo,function(data,success){
			total = data.pageCount;
			if(total){
				let zonename = moment.tz.guess();
				let taskRecords = data.results[0].tasks;
				$(".pending_list").append("<div class='card'><header class='card-header'><p class='card_channel'>"+data.results[0]._id+"</p></header><div class='card-content "+data.results[0]._id+"'>");
				for(x in taskRecords){
					if(/<http.*>/i.test(taskRecords[x].task)){
						let re = /<http.*>/ig;
						let arr;
						let taskWithUrl;
						while((arr = re.exec(taskRecords[x].task)) !== null){
							let arrItem = arr[0];
							if(/<http.*\|.*>/.test(arrItem)){
								taskWithUrl =  taskRecords[x].task.replace(arr[0],'<a href="'+/[^<][^|]*/.exec(arrItem)+'">'+/[^<][^|]*/.exec(arrItem)+'</a>');  
							} else{
								taskWithUrl =  taskRecords[x].task.replace(arr[0],'<a href="'+/[^<].*[^>]/.exec(arrItem)+'">'+/[^<].*[^>]/.exec(arrItem)+'</a>');  
							}

						}
						
						$("."+data.results[0]._id).append('<p class="task">⏳&nbsp;'+taskWithUrl+'&nbsp;<small>'+moment.tz(taskRecords[x].submitDate,zonename).fromNow()+'</small></p>');
					} else {
						
						$("."+data.results[0]._id).append('<p class="task">⏳&nbsp;'+taskRecords[x].task+'&nbsp;<small>'+moment.tz(taskRecords[x].submitDate,zonename).fromNow()+'</small></p>');
					}
				}
				$(".card").append("</div>");
				$(".pending_list").append("</div>");
				console.log(taskRecords);
			}
		});
	}
	$(".joinModalButton").on('click',function(){
      var target = "#"+ $(this).data("target");
      $(target).addClass("is-active");
    });
    $(".modal-background, .modal-close").on('click',function(){
      var target = "#"+ $(this).data("target");
      $(target).removeClass("is-active");
    });
});