$(document).ready(function(){
	let url = $('.done').attr('href');
	let pageNo = 1;
	let total;
	let ajaxRunning = false;
	loadTasks(pageNo);
	pageNo++;
	$(document).ajaxStart(function(e, xhr, opt){
		$(".done_list").append("<div class='has-text-centered spinner_ico'><i class='fas fa-spinner fa-spin'></i></div>");
		ajaxRunning = true;
	}).ajaxStop(function(e, xhr, opt){
		$("div").remove(".spinner_ico");
		ajaxRunning = false;
	});
	console.log("Doc: "+$(document).height()+'\nWin: '+$(window).height()+'\nWin: '+$(window).scrollTop());
	$(window).scroll(function(){
		var docHeight = document.body.offsetHeight;
		docHeight = docHeight == undefined ? window.document.documentElement.scrollHeight : docHeight;

		var winheight = window.innerHeight;
		winheight = winheight == undefined ? document.documentElement.clientHeight : winheight;

		var scrollpoint = window.scrollY;
		scrollpoint = scrollpoint == undefined ? window.document.documentElement.scrollTop : scrollpoint;
		if  (!ajaxRunning && ((scrollpoint + winheight) >= docHeight)){
			if(pageNo>total){
				return false;
			} else {
				loadTasks(pageNo);
				pageNo++;
			}
		} else {return};
	});

	let no_scrollbar_workaround = setInterval(function checkVariable() {

		if($(window).height() >= $(document).height()) {
			if (pageNo>total) {
				clearInterval(no_scrollbar_workaround);
				return false;
			} else {
				loadTasks(pageNo);

				pageNo++;
			}
		}
	}, 1000);
	console.log(url);
	function loadTasks(pageNo){
		$.getJSON(url+'/done?pageNo='+pageNo,function(data,success){
			total = data.pageCount;
			if(total){
				console.log(total+success);
				let yesterday = new Date();
				yesterday.setDate(yesterday.getDate()-1);
				let date;
				let zonename = moment.tz.guess();
				console.log(yesterday.getFullYear()+'-'+(yesterday.getMonth()+1)+'-'+yesterday.getDate())
				if(new Date().toDateString() == new Date(data.results[0]._id.date).toDateString()){
					date = moment.tz(data.results[0]._id.date,zonename).format("[<strong>Today</strong>,] MMMM Do");
				} else if(yesterday.toDateString() == new Date(data.results[0]._id.date).toDateString()) {
					date = moment.tz(data.results[0]._id.date,zonename).format("[<strong>Yesterday</strong>,] MMMM Do");
					console.log(date);
				} else {
					date = moment.tz(data.results[0]._id.date,zonename).format("[<strong>]dddd[</strong>], MMMM Do");
				}
				let taskRecords = data.results[0].tasks;
				$(".done_list").append("<div class='card'><header class='card-header'><p class='card_date'>"+date+"</p></header><div class='card-content "+moment.tz(data.results[0]._id.date,zonename).format("DMY")+"'>");
				for(x in taskRecords){
					if(/<http.*>/i.test(taskRecords[x].task)){
						let re = /<http.*>/ig;
						let arr;
						let taskWithUrl;
						while((arr = re.exec(taskRecords[x].task)) !== null){
							let arrItem = arr[0];
							if(/<http.*\|.*>/.test(arrItem)){
								taskWithUrl =  taskRecords[x].task.replace(arr[0],'<a href="'+/[^<][^|]*/.exec(arrItem)+'">'+/[^<][^|]*/.exec(arrItem)+'</a>');  
							} else{
								taskWithUrl =  taskRecords[x].task.replace(arr[0],'<a href="'+/[^<].*[^>]/.exec(arrItem)+'">'+/[^<].*[^>]/.exec(arrItem)+'</a>');  
							}

						}
						
						
						$("."+moment.tz(data.results[0]._id.date,zonename).format("DMY")).append('<p class="task"><i class="fas fa-check tick_icon"></i>&nbsp;'+taskWithUrl+'&nbsp;<a class="userTask_channel" href="'+data.slackurl+'/messages/'+taskRecords[x].channel[0].channelID+'" target="_blank">#'+taskRecords[x].channel[0].channelName+'</a>&nbsp;<small>'+moment.tz(taskRecords[x].doneDate,zonename).fromNow()+'</small></p>');
					} else {
						$("."+moment.tz(data.results[0]._id.date,zonename).format("DMY")).append('<p class="task"><i class="fas fa-check tick_icon"></i>&nbsp;'+taskRecords[x].task+'&nbsp;<a class="userTask_channel" href="'+data.slackurl+'/messages/'+taskRecords[x].channel[0].channelID+'" target="_blank">#'+taskRecords[x].channel[0].channelName+'</a>&nbsp;<small>'+moment.tz(taskRecords[x].doneDate,zonename).fromNow()+'</small></p>');
					}
					//$("."+moment(data.results[0]._id.date).format("DMY")).append('<p>✅&nbsp;'+taskRecords[x].task+'&nbsp;<a class="userTask_channel">#'+taskRecords[x].channel[0].channelName+'</a>&nbsp;<small>'+moment(taskRecords[x].doneDate).fromNow()+'</small></p>');
				}
				$(".card").append("</div>");
				$(".done_list").append("</div>");
				console.log(taskRecords);
			}
		});
	}
	$(".joinModalButton").on('click',function(){
      var target = "#"+ $(this).data("target");
      $(target).addClass("is-active");
    });
    $(".modal-background, .modal-close").on('click',function(){
      var target = "#"+ $(this).data("target");
      $(target).removeClass("is-active");
    });
});