$(document).ready(function() {
  $(".joinModalButton, .addResourceButton").on("click", function() {
    var target = "#" + $(this).data("target");
    $(target).addClass("is-active");
    if (target === "#joinModal" && $(this).hasClass("addResourceButton")) {
      $(target)
        .find(".join_form_modal_content")
        .prepend(
          '<article class="message is-danger"><div class="message-body">Account required to contribute to resources.</div></article>'
        );
    }
  });
  $(".modal-background, .modal-close").on("click", function() {
    var target = "#" + $(this).data("target");
    $(target).removeClass("is-active");
    if ($(target).find("article.is-danger").length > 0) {
      $(target)
        .find("article.is-danger")
        .remove();
    }
  });

  $(".resource_form").submit(function(event) {
    event.preventDefault();
    var formData = {
      title: $(".resource_form .input[name=title]").val(),
      userid: $(this).data("userid")
    };
    if (formData.title === "") {
      if ($(".main_container").children("article.is-danger").length > 0) {
        return;
      }
      $(".main_container").prepend(
        '<article class="message is-danger"><div class="message-body">Incomplete information. Please fill the fields.</div></article>'
      );
      return;
    } else {
      $(".main_container")
        .children("article.is-danger")
        .remove();
    }
    $(".resource_form")
      .parents("#addResourceModal")
      .toggleClass("is-active");
    $.post("/resources", formData, function(data, status) {
      console.log(data);
      window.location.href = "/resources/" + data.id;
    });
  });
});
