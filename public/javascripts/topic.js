$(document).ready(function () {

	$(document).ajaxStart(function (e, xhr, opt) {
		$(".courses_div").append("<div class='has-text-centered spinner_ico'><i class='fas fa-spinner fa-spin'></i></div>");

	}).ajaxStop(function (e, xhr, opt) {
		$("div").remove(".spinner_ico");

	});
	$.get("/resources/modules?id=" + $(".module_form").data("id"), function (data, status) {
		//console.log(data);
		var modules = data.modules;
		var i, j;
		if (!modules.length) {
			$(".addCourseButton").hide();
		} else {
			$(".addCourseButton").show();
		}
		for (i in modules) {
			if (i == 0) {
				$(".module_list").append('<li data-id="' + modules[i]._id + '"><a class="is-active">' + modules[i].title + '</a></li>');
				for (j in modules[i].courses) {
					$(".courses_div").append('<div class="card"><div class="card-content"><div class="content"><a target="_blank" href="' + modules[i].courses[j].url + '">' + modules[i].courses[j].title + '</a></div></div></div>');
				}
			} else {
				$(".module_list").append('<li data-id="' + modules[i]._id + '"><a>' + modules[i].title + '</a></li>');
			}
		}
	});
	$(".module_form").submit(function (event) {
		event.preventDefault();
		var formData = {
			'title': $('.module_form .input[name=title]').val(),
			'id': $(this).data("id")
		};
		if (formData.title === '') {
			$(".module_form").parents("#addModuleModal").toggleClass("is-active");
			if ($(".main_container").children("article.is-danger").length > 0) {
				return;
			}
			$(".main_container").prepend('<article class="message is-danger"><div class="message-body">Incomplete information. Please fill the fields.</div></article>');
			return;
		} else {
			$(".main_container").children("article.is-danger").remove();
		}
		$(".module_form").parents("#addModuleModal").toggleClass("is-active");

		$.post('/resources/modules', formData, function (data, status) {
			var module = data.module;
			console.log(data);
			if ($(".module_list li a").hasClass("is-active")) {
				$(".module_list").append('<li data-id="' + module._id + '"><a>' + module.title + '</a></li>');
			} else {
				$(".module_list").append('<li data-id="' + module._id + '"><a class="is-active">' + module.title + '</a></li>');
			}
			$(".module_form")[0].reset();
			$(".addCourseButton").show();
		});
	});
	$(".course_form").submit(function (event) {
		event.preventDefault();
		var formData = {
			'title': $('.course_form .input[name=title]').val(),
			'url': $('.course_form .input[name=url]').val(),
			'id': $('.module_list li a.is-active').parent().data("id")
		};
		if (formData.title === '' || formData.url === '') {
			$(".course_form").parents("#addCourseModal").toggleClass("is-active");
			if ($(".main_container").children("article.is-danger").length > 0) {
				return;
			}
			$(".main_container").prepend('<article class="message is-danger"><div class="message-body">Incomplete information. Please fill the fields.</div></article>');
			return;
		} else {
			$(".main_container").children("article.is-danger").remove();
		}
		$(".course_form").parents("#addCourseModal").toggleClass("is-active");
		$.post('/resources/courses', formData, function (data, status) {
			var course = data.course;
			console.log(data);
			$(".courses_div").append('<div class="card"><div class="card-content"><div class="content"><a target="_blank" href="' + course.url + '">' + course.title + '</a></div></div></div>');
			$(".course_form")[0].reset();
			
		});
	});
	var active = false;
	var xhr;
	$(".module_list").on("click", "li", function () {
		console.log($(this));

		if ($(this).children().hasClass("is-active")) {
			return;
		}
		$(".module_list li").children().removeClass("is-active");
		$(this).children().toggleClass("is-active");
		$(".courses_div").empty();
		if (active) { xhr.abort(); }
		active = true;
		xhr = $.get('/resources/courses?moduleId=' + $(this).data("id"), function (data, status) {
			var courses = data.courses;
			var j;
			for (j in courses) {
				$(".courses_div").append('<div class="card"><div class="card-content"><div class="content"><a target="_blank" href="' + courses[j].url + '">' + courses[j].title + '</a></div></div></div>');
			}
			active = false;
		});
	});
	$(".joinModalButton, .addModuleButton, .addCourseButton").on('click', function () {
		var target = "#" + $(this).data("target");
		$(target).addClass("is-active");
		if (target === "#joinModal" && ($(this).hasClass("addModuleButton") || $(this).hasClass("addCourseButton"))) {
			$(target).find(".join_form_modal_content").prepend('<article class="message is-danger"><div class="message-body">Account required to contribute to resources.</div></article>');
		}
	});
	$(".modal-background, .modal-close").on('click', function () {
		var target = "#" + $(this).data("target");
		$(target).removeClass("is-active");
		if ($(target).find("article.is-danger").length > 0) {
			$(target).find("article.is-danger").remove();
		}
	});
	$(".courses_div").on('click', ".courseDone, .coursePending", function () {
		var doneFlag;
		if ($(this).hasClass("courseDone")) {
			doneFlag = true;
			console.log("done");
		} else {
			doneFlag = false;
			console.log("pending");
		}
	});
});