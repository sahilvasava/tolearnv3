const express = require("express");
const router = express.Router();
const request = require("request-promise-native");
const User = require("../models/user");
const Channel = require("../models/channel");
const Task = require("../models/task");
const moment = require("moment");
const Promise = require("promise");
const keys = require("../config/keys");
//const { WebClient } = require('@slack/client');
const { WebClient } = require("@slack/web-api");

const web = new WebClient(keys.slack.slackBotToken);

router.post("/slash", async function (req, res) {
  try {
    let task = req.body.text;

    res.json({
      response_type: "ephemeral",
      text: "Gotcha'!",
    });

    if (/show/i.test(task)) {
      req.url = "/show";
      router.handle(req, res);
    } else {
      let doneFlag = /done\s*:/i.test(task);
      let returnText = doneFlag
        ? req.body.text.replace(/done\s*:\s*/i, "")
        : req.body.text;
      if (/\w+/.test(returnText)) {
        const doc = await createTask(doneFlag, req, res); //.then((doc) => {
        await request.post({
          url: req.body.response_url,
          json: {
            response_type: "in_channel",
            text: doneFlag
              ? ":heavy_check_mark: <@" +
                req.body.user_id +
                "> " +
                "Completed the Task: " +
                returnText +
                " :clap:"
              : ":hourglass_flowing_sand: <@" +
                req.body.user_id +
                "> " +
                "Added Task: " +
                returnText,
          },
        });
      } else {
        await request.post({
          url: req.body.response_url,
          json: {
            response_type: "ephemeral",
            text: "Oops! Invalid input!",
          },
        });
      }
    }
  } catch (error) {
    console.log(error);
  }
});

async function createTask(doneFlag, req, res) {
  try {
    console.log(req.body);
    let text = req.body.text;
    let newTask = new Task();
    const user = await User.find({ slackID: req.body.user_id });
    newTask.user = user[0];

    newTask.submitDate = new Date();
    if (doneFlag) {
      text = text.replace(/done\s*:\s*/i, "");
      newTask.doneDate = new Date();
    }
    if (/<http.*>/.test(text)) {
      console.log("url: " + /<http.*>/.exec(text));
    }
    if (req.body.channel_name != "directmessage") {
      const channel = await Channel.find({ channelID: req.body.channel_id });
      console.log(channel);
      newTask.task = text;
      newTask.channel = channel[0];
      const doc = await newTask.save();
      if (doneFlag) {
        streak(user[0]);
      }
      return doc;
    } else {
      if (/<#.*>\s*/.test(text)) {
        let chId = /#\w*/.exec(text)[0].substring(1);
        text = text.replace(/<#.*>\s*/, "");
        const channel = await Channel.find({ channelID: chId });
        newTask.channel = channel[0];
        newTask.task = text;
        const doc = await newTask.save();

        await web.chat.postMessage({
          channel: chId,
          text: doneFlag
            ? ":heavy_check_mark: <@" +
              req.body.user_id +
              "> " +
              "Completed the Task: " +
              text +
              " :clap:"
            : ":hourglass_flowing_sand: <@" +
              req.body.user_id +
              "> " +
              "Added Task: " +
              text,
        });
        if (doneFlag) {
          streak(user[0]);
        }
        return doc;
      } else {
        const channel = await Channel.find({ channelName: "general" });
        newTask.task = text;
        newTask.channel = channel[0];
        const doc = await newTask.save();
        if (doneFlag) {
          streak(user[0]);
        }
        return doc;
      }
    }
  } catch (error) {
    console.log(error);
  }
}

async function streak(userId) {
  try {
    const taskCount = await Task.countDocuments({
      user: userId._id,
      doneDate: {
        $gte: moment().startOf("day"),
        $lt: moment().endOf("day"),
      },
    });
    console.log(`taskCount ${taskCount}`);
    if (taskCount === 1) {
      await userId.updateOne({ $inc: { streak: 1 } });
    }
  } catch (error) {
    console.log(error);
  }
}

router.post("/show", async function (req, res) {
  try {
    console.log("Show" + req.body.text);
    console.log(req.body.channel_name);
    if (req.body.channel_name != "directmessage") {
      const userRes = await User.find({ slackID: req.body.user_id });
      const channelRes = await Channel.find({ channelID: req.body.channel_id });
      const task = await Task.find({
        user: userRes[0].id,
        channel: channelRes[0].id,
        doneDate: null,
      });
      console.log(`Task array ${task}`);
      const taskArr = task.map((taskEl) => {
        return { text: taskEl.task, value: taskEl.task };
      });
      console.log(taskArr);
      const chatTListRes = await web.chat.postEphemeral({
        user: req.body.user_id,
        channel: req.body.channel_id,
        text: "Here's your task list:",
        attachments: [
          {
            text: "Select a task to mark as done:",
            fallback: "Options",
            callback_id: "tasks",
            actions: [
              {
                name: "Tasks",
                text: "Select a task",
                type: "select",
                option_groups: [
                  {
                    text: "#" + req.body.channel_name.toUpperCase(),
                    options: taskArr,
                  },
                ],
              },
            ],
          },
        ],
      });
      console.log("Message sent: ", chatTListRes);
    } else {
      const userRes = await User.find({ slackID: req.body.user_id });

      const task = await Task.aggregate([
        { $match: { user: userRes[0]._id, doneDate: null } },
        {
          $group: {
            _id: "$channel",
            records: {
              $push: "$task",
            },
            count: {
              $sum: 1,
            },
          },
        },
        {
          $lookup: {
            from: "channels",
            localField: "_id",
            foreignField: "_id",
            as: "channel_doc",
          },
        },
      ]).exec();
      console.log(task);
      const taskGroupArr = task.map((taskEl) => {
        return {
          text: taskEl.channel_doc.length
            ? "#" + taskEl.channel_doc[0].channelName.toUpperCase()
            : "anon",
          options: taskEl.records.map((rec) => {
            return { text: rec, value: rec };
          }),
        };
      });
      console.log(taskGroupArr);
      console.log(req.body);
      await request.post({
        url: req.body.response_url,
        json: {
          text: "Here's your task list:",
          attachments: [
            {
              fallback: "Options mannn",
              callback_id: "tasks",
              text: "Select a task to mark as done:",
              actions: [
                {
                  name: "Tasks",
                  text: "Select a task",
                  type: "select",
                  option_groups: taskGroupArr,
                },
              ],
            },
          ],
        },
      });
    }
  } catch (error) {
    console.log(error);
  }
});

router.post("/interactive", async function (req, res) {
  try {
    const payload = JSON.parse(req.body.payload);
    const userRes = await User.findOne({ slackID: payload.user.id });
    const taskRes = await Task.find({
      user: userRes._id,
      task: payload.actions[0].selected_options[0].value,
    });

    taskRes[0].doneDate = new Date();
    const doc = await taskRes[0].save();

    streak(userRes);

    const taskDoneRes = await web.chat.postEphemeral({
      user: payload.user.id,
      channel: payload.channel.id,
      text: ":heavy_check_mark: Task: *" + doc.task + "* marked as done :clap:",
    });
    console.log(`Task marked done: ${taskDoneRes}`);
    const channelRes = await Channel.find({ _id: doc.channel });
    const taskCompMessRes = await web.chat.postMessage({
      channel: channelRes[0].channelID,
      text:
        ":heavy_check_mark: <@" +
        payload.user.id +
        "> " +
        "Completed the Task: " +
        doc.task +
        " :clap:",
    });
    console.log("Message sent: ", taskCompMessRes);
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
