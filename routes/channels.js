const express = require("express");
const router = express.Router();
const User = require("../models/user");
const Channel = require("../models/channel");
const keys = require("../config/keys");
//const { WebClient } = require('@slack/client');
const { WebClient } = require("@slack/web-api");

const web = new WebClient(keys.slack.slackInviteToken);

router.post("/events", (req, res) => {
  const { event, type } = req.body;
  switch (type) {
    case "url_verification": {
      // verify Events API endpoint by returning challenge if present
      res.send({ challenge: req.body.challenge });
      break;
    }
    case "event_callback": {
      res.sendStatus(200);
      console.log(req.body);
      switch (event.type) {
        case "channel_created": {
          channelCreated(event);
          break;
        }
        case "member_joined_channel": {
          // (TODO) Figure out what to do with admin account
          if (event.user !== "DA0T5JZT5") memberJoinedChannel(event);
          break;
        }
        case "channel_deleted": {
          channelDeleted(event);
          break;
        }
        case "member_left_channel": {
          memberLeftChannel(event);
          break;
        }
        default: {
          break;
        }
      }
      break;
    }
    default: {
      res.sendStatus(500);
      break;
    }
  }
});
async function channelCreated(event) {
  try {
    web.conversations.join({ channel: event.channel.id });

    const channel = await Channel.findOne({ channelID: event.channel.id });
    if (!channel) {
      const channelDoc = await Channel.create({
        channelName: event.channel.name,
        channelID: event.channel.id,
      });
      console.log(channelDoc);

      await User.findOneAndUpdate(
        { slackID: event.channel.creator },
        { $addToSet: { channel: channelDoc } }
      );
    }
  } catch (error) {
    console.log(error);
  }
}
async function memberJoinedChannel(event) {
  try {
    const channelRes = await Channel.findOne({ channelID: event.channel });
    console.log("channelRes: " + channelRes);
    if (err) return handleError(err);
    if (channelRes) {
      await User.findOneAndUpdate(
        { slackID: event.user },
        { $addToSet: { channel: channelRes } }
      );
    }
  } catch (error) {
    console.log(error);
  }
}

async function channelDeleted(event) {
  try {
    const channelDoc = await Channel.findOne({ channelID: event.channel });
    if (channelDoc) {
      await User.updateMany(
        {},
        { $pull: { channel: channelDoc.id } },
        { multi: true }
      );
      await channelDoc.remove();
    }
  } catch (error) {
    console.log(error);
  }
}

async function memberLeftChannel(event) {
  try {
    const channelDoc = await Channel.findOne({ channelID: event.channel });
    if (channelDoc) {
      await User.findOneAndUpdate(
        { slackID: event.user },
        { $pull: { channel: channelDoc.id } }
      );
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = router;
