const express = require("express");
const router = express.Router();
const Channel = require("../models/channel");
const User = require("../models/user");
const Task = require("../models/task");
const keys = require("../config/keys");
/* GET home page. */

/*router.param('slackid', function (req, res, next, slackid) {
  // ... Perform database query and
  // ... Store the user object from the database in the req object
  User.find({slackID:slackid},(err,user)=>{
  	if(err){res.json(err);}
  	req.
  });
  console.log('userreq'+req.user);
  //req.slackid = slackid;
  if(req.user && req.user.slackID == slackid){
  	console.log("user:"+req.user);
  	return next();
  } else {

  }
  req.slackid = slackid;
  console.log(req.slackid);
  return next();
});*/
router.param("slackid", async (req, res, next, val) => {
  try {
    const userP = await User.findOne({ slackID: val });
    if (userP) {
      next();
    } else {
      const err = new Error("Not Found");
      err.status = 404;
      next(err);
    }
  } catch (error) {
    res.json(error);
  }
});

router.get("/about", (req, res) => {
  res.render("about");
});

router.get("/:slackid", async function (req, res, next) {
  try {
    console.log("get" + req.params.slackid);
    const user = await User.findOne({ slackID: req.params.slackid })
      .populate("channel")
      .exec();
    res.render("profile", { userPro: user, slackurl: keys.slack.slackUrl });
  } catch (error) {
    res.json(error);
  }
});

router.get("/:slackid/pending", async function (req, res, next) {
  try {
    console.log("get" + req.params.slackid);
    const user = await User.findOne({ slackID: req.params.slackid })
      .populate("channel")
      .exec();
    res.render("pending", { userPro: user, slackurl: keys.slack.slackUrl });
  } catch (error) {
    res.json(err);
  }
});

router.get("/:slackid/done", async function (req, res) {
  console.log(req.params.slackid);

  const userRes = await User.findOne({ slackID: req.params.slackid });
  if (err) {
    res.json(err);
  }
  let aggregate = Task.aggregate([
    {
      $match: { user: userRes._id, doneDate: { $ne: null } },
    },
    {
      $lookup: {
        from: "channels",
        localField: "channel",
        foreignField: "_id",
        as: "channel",
      },
    },
    {
      $sort: {
        doneDate: -1,
      },
    },
    {
      $group: {
        _id: {
          date: {
            $dateToString: { format: "%Y-%m-%d", date: "$doneDate" },
          },
        },
        tasks: {
          $push: "$$ROOT",
        },
      },
    },
    {
      $sort: {
        _id: -1,
      },
    },
    {
      $project: {
        "tasks.doneDate": 1,
        "tasks.task": 1,
        "tasks.channel.channelName": 1,
        "tasks.channel.channelID": 1,
      },
    },
  ]);
  //aggregate.exec((err,tasks)=>{console.log(JSON.stringify(tasks))});
  Task.aggregatePaginate(
    aggregate,
    { page: parseInt(req.query.pageNo), limit: 1 },
    function (err, results, pageCount, count) {
      if (err) {
        console.log(err);
      } else {
        if (!count) {
          pageCount = 0;
        }
        res.json({
          results,
          pageNo: req.query.pageNo,
          pageCount,
          slackurl: keys.slack.slackUrl,
        });
        console.log(JSON.stringify(results) + "\n\n" + pageCount + count);
      }
    }
  );
});

router.get("/:slackid/pendingtasks", (req, res) => {
  User.findOne({ slackID: req.params.slackid }, (err, userRes) => {
    if (err) {
      res.json(err);
    }
    let aggregate = Task.aggregate([
      {
        $match: { user: userRes._id, doneDate: null },
      },
      {
        $lookup: {
          from: "channels",
          localField: "channel",
          foreignField: "_id",
          as: "channel",
        },
      },
      {
        $group: {
          _id: { $arrayElemAt: ["$channel.channelName", 0] },
          tasks: {
            $push: "$$ROOT",
          },
        },
      },
      {
        $project: {
          "tasks.submitDate": 1,
          "tasks.task": 1,
        },
      },
    ]);
    //aggregate.exec((err,tasks)=>{console.log(JSON.stringify(tasks))});
    Task.aggregatePaginate(
      aggregate,
      { page: parseInt(req.query.pageNo), limit: 1 },
      function (err, results, pageCount, count) {
        if (err) {
          console.log(err);
        } else {
          if (!count) {
            pageCount = 0;
          }
          res.json({
            results,
            pageNo: req.query.pageNo,
            pageCount,
            slackurl: keys.slack.slackUrl,
          });
          console.log(JSON.stringify(results) + "\n\n" + pageCount + count);
        }
      }
    );
  });
});

router.get("/", function (req, res, next) {
  Channel.find({}, (err, channels) => {
    if (err) {
      res.json(err);
    }
    let aggregate = User.aggregate([
      {
        $sort: {
          streak: -1,
        },
      },
      {
        $project: {
          img: 1,
          streak: 1,
          name: 1,
          slackID: 1,
        },
      },
    ]);
    User.aggregatePaginate(
      aggregate,
      { page: 1, limit: 20 },
      function (err, usersStreak, pageCount, count) {
        if (err) {
          console.log(err);
        } else {
          console.log(
            "\n\nUserstreaks: " + JSON.stringify(usersStreak) + "\n\n"
          );
          res.locals.usersStreak = usersStreak;
          res.locals.channels = channels;
          res.render("index");
        }
      }
    );
  });
});

module.exports = router;
