const express = require("express");
const router = express.Router();
const Resource = require("../models/resource");
const Module = require("../models/module");

router.post("/", function(req, res) {
  let resource = new Resource();
  resource.title = req.body.title;
  resource.created_by = req.body.userid;
  console.log(req.body);
  resource.save((err, doc) => {
    if (err) {
      return console.log(err);
    }
    console.log(doc);
    res.json({ id: doc.id });
  });
});
router.get("/modules", (req, res) => {
  //console.log(req.query.id);
  Resource.findOne({ _id: req.query.id })
    .sort({ created_at: -1 })
    .populate("modules")
    .exec((err, resDoc) => {
      if (err) {
        return console.log(err);
      }
      console.log(resDoc.modules);
      res.json({ modules: resDoc.modules });
    }); /*
	Module.find({},(err,modules)=>{
		if(err){
			return console.log(err);
		}
		res.json({modules});
	});*/
});
router.post("/modules", (req, res) => {
  let module = new Module();
  module.title = req.body.title;
  module.save((err, doc) => {
    if (err) {
      return console.log(err);
    }
    Resource.findOne({ _id: req.body.id }, (err, resDoc) => {
      resDoc.update({ $addToSet: { modules: doc } }, (err, updated) => {
        res.json({ module: doc });
      });
    });
  });
});
router.post("/courses", (req, res) => {
  let course = { title: req.body.title, url: req.body.url };
  Module.findOne({ _id: req.body.id }, (err, doc) => {
    doc.update({ $addToSet: { courses: course } }, (err, updated) => {
      res.json({ course: course });
    });
  });
});
router.get("/courses", function(req, res) {
  Module.findOne({ _id: req.query.moduleId }, (err, doc) => {
    if (err) {
      return console.log(err);
    }
    res.json({ courses: doc.courses });
  });
});
router.get("/:topicid", function(req, res) {
  Resource.findOne({ _id: req.params.topicid }, (err, doc) => {
    if (err) return console.log(err);
    res.render("topic", { topic: doc });
  });
});

router.get("/", function(req, res) {
  Resource.find({})
    .populate("created_by")
    .exec((err, resources) => {
      if (err) return console.log(err);
      res.render("resources", { resources: resources });
    });
});
module.exports = router;
