const express = require('express');
const router = express.Router();
const Task = require('../models/task');
const keys = require('../config/keys');
router.post('/fetch',(req,res)=>{
	console.log('fetch');
	

	let aggregate = Task.aggregate([{ $match : { doneDate: { $ne: null} } },
   {
    $lookup: {
        from: "channels",
        localField: "channel",
        foreignField: "_id",
        as: "channel"
    }
},
{
   $lookup: {
    from: "users",
    localField: "user",
    foreignField: "_id",
    as: "user"
}	
},
{
    $sort: {
       "user._id": -1,doneDate: -1
   }
},
{
    $group: {
        _id: { date: { $dateToString: {format:"%Y-%m-%d", date:"$doneDate"}}, user_id: "$user"},
        "tasks": {
            $push: "$$ROOT"
        },
        "count": {
            $sum: 1
        }
    }
},
{
    $sort: {
        "tasks.doneDate":-1
    }
},
{
    $group: {
        _id: { date: "$_id.date"},
        "records": {
            $push: "$$ROOT"
        },
        "count": {
            $sum: 1
        }
    }
},
{
    $project: {
        "records._id.user_id.name": 1,"records._id.user_id.img": 1,"records._id.user_id.slackID": 1, "records.tasks.task": 1, "records.tasks.doneDate": 1, "records.tasks.channel.channelName": 1, "records.tasks.channel.channelID": 1
    }
},
{
    $sort: {
        "_id.date": -1
    }
}

]);  
    console.log(JSON.stringify(req.query));
    Task.aggregatePaginate(aggregate,{page:parseInt(req.query.pageNo), limit: 1}, function(err, results, pageCount, count) {
      if(err) 
      {
        console.log(err)
    }
    else
    { 

        res.json({results, pageNo: req.query.pageNo, pageCount,slackurl:keys.slack.slackUrl});
        console.log(JSON.stringify(results)+'\n\n'+pageCount+count);
    }
});

});







module.exports = router;
