const express = require('express');
const router = express.Router();
const passport = require('passport');
const keys = require('../config/keys');
const request = require('request');
const { body, oneOf, validationResult } = require('express-validator/check');
const { sanitize } = require('express-validator/filter');
router.get('/login', passport.authenticate('slack', {
	scope: ['identity.basic', 'identity.email', 'identity.team', 'identity.avatar']
}));

router.get('/logout', (req, res) => {
	req.logout();
	res.redirect('/');
});

router.get('/login/callback',
	passport.authenticate('slack', { failureRedirect: '/' }),
	function (req, res) {
		// Successful authentication, redirect home.
		res.redirect('/');
		console.log('callback');
	});

router.post('/join', [body('email').isEmail().withMessage('Enter valid email').trim()], (req, res) => {
	console.log(req.body.email);
	const errors = validationResult(req);
	console.log(errors.isEmpty() + JSON.stringify(errors.array()));
	if (errors.isEmpty()) {
		request.post({
			url: keys.slack.slackUrl + '/api/users.admin.invite',
			//url: keys.slack.slackUrl + '/api/admin.users.invite',
			form: {
				email: req.body.email,
				token: keys.slack.slackInviteToken,
				set_active: true,
				resend: true
			}
		}, function (err, httpResponse, body) {
			// body looks like:
			//   {"ok":true}
			//       or
			//   {"ok":false,"error":"already_invited"}
			if (err) {
				console.log(err);
				res.send('Error:' + err);
			}
			body = JSON.parse(body);
			if (body.ok) {
				console.log('invitation sent');
				req.flash('success', 'Check ' + req.body.email + ' for an invite from Slack.');
				res.redirect('/');
			} else {
				let error = body.error;
				console.log(error);
				req.flash('danger', 'Invitation Failed!');
				res.redirect('/');
			}
		});
	} else {
		var x;
		var errArr = errors.array();
		for (x in errArr) {
			req.flash('danger', errArr[x].msg);
		}


		res.redirect('/');
	}
});

module.exports = router;
