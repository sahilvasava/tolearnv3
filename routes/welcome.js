const express = require("express");
const router = express.Router();
const User = require("../models/user");
const Channel = require("../models/channel");
const Promise = require("promise");
//const { WebClient } = require('@slack/client');
const { WebClient } = require("@slack/web-api");
const keys = require("../config/keys");
const web = new WebClient(keys.slack.slackBotToken);
const web2 = new WebClient(keys.slack.slackInviteToken);
const request = require("request");

router.post("/events", (req, res) => {
  //console.log(req);
  //console.log(JSON.stringify(req.body));
  const event = req.body.event;
  new Promise((resolve, reject) => {
    res.sendStatus(200);
    return resolve();
  }).then(() => {
    Channel.findOne({ channelName: "general" }, (err, channelRes) => {
      console.log("Channel find:" + channelRes);
      new User({
        slackID: event.user.id,
        name: event.user.real_name,
        email: event.user.profile.email,
        img: event.user.profile.image_512,
      })
        .save()
        .then((userRes) => {
          console.log("user create:" + userRes);
          web.users.info({ user: event.user.id }).then((res) => {
            console.log("web user:" + res);
            User.update(
              { _id: userRes.id },
              {
                email: res.user.profile.email,
                $addToSet: { channel: channelRes },
              },
              (err, updated) => {
                console.log(updated);
                //web.im.open({ user: event.user.id }).then((resIm) => {
                web.conversations
                  .open({ users: event.user.id })
                  .then((resIm) => {
                    console.log("Im open:" + resIm);
                    web.chat
                      .postMessage({
                        channel: resIm.channel.id,
                        text:
                          "Hey <@" +
                          event.user.id +
                          ">, :wave:! Welcome to the *Remote Learners Community*. We are a community of self-directed learners from diverse fields learning together and keeping each other accountable to learn everyday.",
                        attachments: [
                          {
                            text: "Here are all topic channels which you can join:",
                            fallback:
                              "Upgrade your Slack client to use messages like these.",
                            color: "#3AA3E3",
                            attachment_type: "default",
                            callback_id: "channels",
                            actions: [
                              {
                                name: "channels_list",
                                text: "Choose a channel to join",
                                type: "select",
                                data_source: "channels",
                              },
                            ],
                          },
                          {
                            text: "Create a new channel from the left sidebar if it already does not exists.",
                            color: "#3AA3E3",
                          },
                          {
                            text: "Come back once you join or create a channel and click this button below to know how to create tasks.",
                            fallback: "You are unable to choose a game",
                            callback_id: "howtotasks",
                            color: "#3AA3E3",
                            attachment_type: "default",
                            actions: [
                              {
                                name: "infotasks",
                                text: "Click me",
                                type: "button",
                                value: "infotasks",
                              },
                            ],
                          },
                        ],
                      })
                      .then((resMess) => {
                        console.log("Message sent:" + resMess);
                      });
                  });
              }
            );
          });
        });
      /*User.create({slackID: event.user.id, name: event.user.real_name, email: event.user.profile.email, img: event.user.profile.image_512},(err,userRes)=>{
				console.log("user create:"+userRes);
				web.users.info({user:event.user.id}).then((res)=>{
					console.log("web user:"+res);
					User.update({_id:userRes.id},{email:res.user.profile.email, "$addToSet": { channel: channelRes } },(err,updated)=>{
					console.log(updated);
				});
				});
			});*/
    });
  });
});

router.post("/interactive", (req, res) => {
  //console.log("welcome interactive");
  let payload = JSON.parse(req.body.payload);
  web.conversations.open({ users: payload.user.id }).then((resIm) => {
    if (payload.callback_id == "channels") {
      let channelid = payload.actions[0].selected_options[0].value;
      //web2.channels
      web2.conversations
        .invite({ channel: channelid, users: payload.user.id })
        .then((inviteRes) => {
          console.log("User invited:" + inviteRes);
        });
      web.chat
        .postMessage({
          channel: resIm.channel.id,
          text: "You just joined <#" + channelid + ">",
        })
        .then((resMess) => {
          console.log("Message sent:" + resMess);
        });
    } else if (payload.callback_id == "howtotasks") {
      web.chat
        .postMessage({
          channel: resIm.channel.id,
          text: "Here is a quick guide of what I can do:\n• Use `/remote <your task>` in a channel to create a new task\n• Use `/remote done: <your task>` in a channel to create task and immediately mark it as done\n• Use `/remote show` to see all the pending tasks in a channel and mark them as completed\n• To create tasks from direct messages with other person or with `@Remote Learners` just use `#channelname` in above commands, if you don't use tag then by default task will be created in `#general` channel\n",
        })
        .then((resMess) => {
          console.log("Message sent:" + resMess);
        });
    }
  });
});

module.exports = router;
