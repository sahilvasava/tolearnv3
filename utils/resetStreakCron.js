const CronJob = require("cron").CronJob;
const User = require("../models/user");
const Task = require("../models/task");

const resetStreak = async () => {
  try {
    let now = new Date();
    let yesterday = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate() - 1,
      0,
      0,
      0
    );

    const tasks = await Task.aggregate([
      {
        $match: { doneDate: { $ne: null } },
      },
      {
        $project: {
          date: {
            $dateToString: { format: "%Y-%m-%d", date: "$doneDate" },
          },
          user: 1,
        },
      },
      {
        $group: {
          _id: "$user",
          tasks: {
            $push: "$$ROOT",
          },
        },
      },
      {
        $match: {
          "tasks.date": { $ne: yesterday.toISOString().split("T")[0] },
        },
      },
      {
        $group: {
          _id: "$_id",
        },
      },
    ]).exec();
    await User.updateMany({ _id: { $in: tasks } }, { streak: 0 });
    console.log("updated");
  } catch (error) {
    console.log(error);
  }
};

// Runs resetStreak every midnight
const job = new CronJob("00 00 00 * * *", resetStreak);

module.exports = job;
